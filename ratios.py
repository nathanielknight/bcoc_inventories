#!/usr/bin/python

import data
import helpers.ij_tools as ij
import regrid as rg
import gc_helpers as gch
from os.path import join

import gchem.grids as gc_grids
import numpy as np

perioddata = list(data.iter_run_periods())
plt_dir = "./plt/ratios/"
lon = np.column_stack([gc_grids.e_lon_2x25 
                       for i in range(gc_grids.e_lat_2x25.shape[0])])
lat = np.row_stack([gc_grids.e_lat_2x25
                    for i in range(gc_grids.e_lon_2x25.shape[0])])


def pluck(dict, *args):
    return (dict[arg] for arg in args)


def plot_ratio(bc_ems, oc_ems, sector, year, filename):
    ratio =ij.map_ij(gch.divider(0), bc_ems, oc_ems)
    gch.plot_ij(ratio,
              filename=filename,
              title="BC/OC Ratio {} {}".format(sector, year),
              vmin=1e-2, vmax=100,
                lon=lon, lat=lat,
              norm=gch.LogNorm())

def save_ratio(sector, filename):
    bc_ems, oc_ems, sector, year = map(lambda d: pluck(d, "bc_data", "oc_data", "sector", "year"),
                                       data.iter_sectoral_periods(sector))[0]
    #bc and oc _ems shape is a little off (because we can't just pick
    #one 2x2.5 grid configuration and use it consistently. OH NO. THAT
    #WOULD BE CRAZY PANTS.) To compensate, add an extra column of
    #zeroes at the end. It shouldn't affect the results much and it's
    #in the middle of the pacific where we don't care about it.
    bc_ems = ij.array_to_ij(rg.convert_2to4(bc_ems))
    oc_ems = ij.array_to_ij(rg.convert_2to4(oc_ems))
    r = ij.map_ij(gch.divider(0), bc_ems, oc_ems)
    ij.write_ij(r, filename)


#Numpy-array hacking to get ratios into 4x5 grid
save_ratio("fossil", "bcoc_fossil.dat")
save_ratio("biofuel", "bcoc_biofuel.dat")


#Make plots if 
if __name__ == "__main__":
    for bc_ems, oc_ems, sector, year in map(lambda d: pluck(d, "bc_data", "oc_data", "sector", "year"),
                                            data.iter_sectoral_periods("fossil")):
        filename = join(plt_dir, "{}_{}_ratio.png".format(sector, year))
        bc_ems = ij.array_to_ij(bc_ems)
        oc_ems = ij.array_to_ij(oc_ems)
        plot_ratio(bc_ems, oc_ems, sector, year, filename)

        
    for bc_ems, oc_ems, sector, year in map(lambda d: pluck(d, "bc_data", "oc_data", "sector", "year"),
                                            data.iter_sectoral_periods("biofuel")):
        filename = join(plt_dir, "{}_{}_ratio.png".format(sector, year))
        bc_ems = ij.array_to_ij(bc_ems)
        oc_ems = ij.array_to_ij(oc_ems)
        plot_ratio(bc_ems, oc_ems, sector, year, filename)
