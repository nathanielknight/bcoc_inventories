#!/usr/bin/python
'''
functions for regridding geos-chem data from 2x2.5 to 4x5 resolution
'''

import numpy as np
import helpers.seqtools as s
import helpers.ij_tools as ij
import gchem.grids as ggrids
import gc_helpers as h
import matplotlib.pyplot as plt




#Helpers
split = lambda x, n: [x/float(n) for i in range(n)]


def upsample_lon2(col):
    '''Split each 2.5 degree lon cell into 10 .25 degree lon cells.'''
    return s.mapcat(lambda x: split(x, 10), col)

def upsample_lat2(arr):
    '''Split each 2 degree lat cell into 8 .25 degree lat cells.'''
    first = split(arr[0], 4)
    last = split(arr[-1], 4)
    middle = s.mapcat(lambda x: split(x, 8), arr[1:-1])
    return np.array(first+middle+last)

def upsample2(arr):
    arr = np.column_stack([upsample_lon2(col)
                         for col in [arr[:,i] 
                                     for i in range(arr.shape[1])]])
    arr = np.row_stack([upsample_lat2(row)
                        for row in [arr[i,:]
                                    for i in range(arr.shape[0])]])
    return arr


def downsample_lon5(col):
    '''Join 20 .25 lon cells into one 5 lon cell.'''
    return np.array(map(sum, s.partition(col, 20)))

def downsample_lat4(arr):
    '''Join 16 .25 lat cell into one 4 lat cell.'''
    first = sum(arr[:8])
    last = sum(arr[-8:])
    middle = map(sum, s.partition(arr[8:-8], 16))
    return np.array([first]+middle+[last])
    


def downsample4(arr):
    arr = np.roll(arr, 5, 0)
    x = np.column_stack([downsample_lon5(col)
                        for col in [arr[:,i]
                                    for i in range(arr.shape[1])]])
    x = np.row_stack([downsample_lat4(row)
                      for row in [x[i,:]
                                  for i in range(x.shape[0])]])
    return x
    


def convert_2to4(arr):
    return downsample4(upsample2(arr))

if __name__ == "__main__":
    #Test ; remove exit() to make plots of before, during, and after conversion
    x = np.loadtxt("testdata.csv", delimiter=',')
    # plt.imshow(x)
    # plt.savefig("plt/testdata.png")
    x = upsample2(x)
    plt.imshow(x)
    plt.savefig("plt/upsampled.png")
    x = np.roll(x, 5, 0)
    x = downsample4(x)
    plt.imshow(x)
    plt.savefig("plt/downsampled.png")
