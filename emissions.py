#!/usr/bin/python
'''\
Plot emissions of BC and OC by sector for all years in inventory.
'''
import data
import helpers.ij_tools as ij
import numpy as np
import gc_helpers as gch

from gchem import grids as gc_grids
from os.path import join

#Constants
plt_dir = "./plt/bcoc_inventory_emissions"
lon = np.column_stack([gc_grids.e_lon_2x25 for i in range(gc_grids.e_lat_2x25.shape[0])])
lat = np.row_stack([gc_grids.e_lat_2x25 for i in range(gc_grids.e_lon_2x25.shape[0])])


for da in data.iter_whole_inventory():
    data_ij = ij.array_to_ij(da["arr"])
    title = "{sector} {spcs} {date}".format(**da)
    cblabel = "emissions [kg/box]"
    filename = join(plt_dir, "{sector}_{spcs}_{date}.png".format(**da))
    gch.plot_ij(data_ij, filename=filename,
                lon=lon, lat=lat,
                norm=gch.LogNorm(),
                vmin=1, vmax=1e6,
                title=title, cblabel=cblabel)
