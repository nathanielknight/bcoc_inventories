"""\
Expose BOND carbon data as a dict-like structure.
"""

import re, os
import gchem as gc
import helpers as h
import itertools as it
import numpy as np

#Load data=================================================
class BPCHDir(h.datadir.DataDir):
    def __init__(self, dirname, filter_by=None):
        matchgeos = lambda n: re.search("geos", n) != None
        if filter_by:
            f = lambda fn: filter_by(fn) and matchgeos(fn)
        else:
            f = matchgeos
        super(BPCHDir, self).__init__(dirname,
                                      filter_by=f)
    
    def __load__(self, filename):
        return gc.bpch.File.fromfile(
            os.path.join(self.dirname, filename),
            mode='r', 
            tracerinfo=os.path.join(self.dirname, "tracerinfo.dat"),
            diaginfo=os.path.join(self.dirname, "diaginfo.dat"))

datadir = BPCHDir("./raw")



#Structured access========================================
def get_datablock(date=None, spcs=None, sector=None, dd=datadir):
    yr = date[:4]
    bpchfs = datadir.search(yr).search(sector).values()
    try:
        return bpchfs[0].filter(name=spcs, time=date)[0]
    except IndexError as e:
        raise e


def params():
    for year in range(1997,2004):
        dates = ["{}-12-01".format(year-1), "{}-01-01".format(year),
                 "{}-02-01".format(year)]
        for date in dates:
            for spcs in ["BCPI", "OCPI"]:
                for sector in ["fossil", "biofuel"]:
                    yield (date, spcs, sector)


def iter_run_periods():
    for spcs, sector in it.product(["BCPI", "OCPI"], ["fossil", "biofuel"]):
        for year in range(1990, 2005):
            dates = ["{}-12-01".format(year-1), "{}-01-01".format(year),
                     "{}-02-01".format(year)]
            try:
                dbs = [get_datablock(date, spcs, sector).value[:,:,0] for date in dates]
                yield {"arr": sum(dbs),
                       "year": year,
                       "sector": sector,
                       "spcs": spcs}
            except IndexError:
                continue


def iter_sectoral_periods(sector):
    for year in range(1990, 2005):
        dates = ["{}-12-01".format(year-1), "{}-01-01".format(year),
                 "{}-02-01".format(year)]
        try:
            bc_dbs = [get_datablock(date, "BCPI", sector).value[:,:,0] for date in dates]
            oc_dbs = [get_datablock(date, "OCPI", sector).value[:,:,0] for date in dates]
            yield {"bc_data": sum(bc_dbs),
                   "oc_data": sum(oc_dbs),
                   "sector": sector,
                   "year": year}
        except IndexError:
            continue


def iter_whole_inventory():
    for date, spcs, sector in params():
        yield {"arr": get_datablock(date, spcs, sector).value[:,:,0],
               "date": date,
               "sector": sector,
               "spcs": spcs}


#---------------------------------------------------------------------
def pluck(d, *args):
    return (d[a] for a in args)

if __name__ == "__main__":
    bc_data, oc_data, sector, year = pluck(iter_sectoral_periods("fossil").next(),
                                           "bc_data", "oc_data", "sector", "year")
    np.savetxt("bc.fossil.csv", bc_data, delimiter=",")
    np.savetxt("oc.fossil.csv", oc_data, delimiter=",")
    bc_data, oc_data, sector, year = pluck(iter_sectoral_periods("biofuel").next(),
                                           "bc_data", "oc_data", "sector", "year")
    np.savetxt("bc.biofuel.csv", bc_data, delimiter=",")
    np.savetxt("oc.biofuel.csv", oc_data, delimiter=",")
